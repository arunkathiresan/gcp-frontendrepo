import React, { Component } from 'react'
import './App.css';
import FileUpload from './components/fileUpload';
import ListFiles from './components/listFiles';
import { BrowserRouter,Switch,Route } from 'react-router-dom';
import Header from './components/header';


class App extends Component {
    
    render() {
        return (
            <div>
             <BrowserRouter>
             <Header/>
             <Switch>
                <Route path="/" exact component={FileUpload}/>
                <Route path="/view" exact component={ListFiles}/>
                </Switch>
                </BrowserRouter>
                
            </div>
        )
    }
}
export default App;