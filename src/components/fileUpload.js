import React, { Component } from 'react'
import axios from 'axios';
import { Progress } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class FileUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null
        }

    }
    onChangeHandler = event => {
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
        })
    }
    onClickHandler = () => {
        const data = new FormData()
        data.append('file', this.state.selectedFile)
        axios.post("http://localhost:9001/uploads", data, {
            onUploadProgress: ProgressEvent => {
                this.setState({
                    loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
                })
            },
        })
            .then(res => {
                // this.setState({selectedFile:null});
                toast.success('uploaded successfully');
                this.setState({loaded:0});

            })
            .catch(err => {
                toast.error("uploading failed");
            })
    }
    render() {
        return (
            <div className="container">
                <div className="form-group">
                    <ToastContainer />
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <form method="post">
                            <div className="form-group files">
                                <label>Upload Your File </label>
                                <br/>
                                <input type="file" name="file" onChange={this.onChangeHandler} required="required" />
                            </div>
                            <div className="form-group">
                                <Progress max="100" color="success" value={this.state.loaded} >{Math.round(this.state.loaded, 2)}%</Progress>
                            </div>
                            <button type="button" className="btn btn-success btn-block" onClick={this.onClickHandler}>Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default FileUpload;