import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './header.css';
class Header extends Component{
    render(){
        return(
            <div className="container">
                <div className="header">
                <span className="heading">Google File Upload</span>
                <span className="link">
                <Link to="/view"><button className="btn btn-primary btn-sm">View Files</button></Link>
                </span>
                <span className="link">
                <Link to="/"><button className="btn btn-primary btn-sm">Upload</button></Link>
                </span>
                </div>
            </div>
        )
    }
}
export default Header;