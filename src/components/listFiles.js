import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import filesize from 'filesize';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class ListFiles extends Component{
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            bucket:"gcp-react",
            fileDelete:false
        };
    }
    componentDidMount() {
       this.getFile();
    }
    getFile=(e)=>{
        axios.get(`http://localhost:9001/view`)
        .then(response => {
            console.log(response.data);
            this.setState({ items: response.data });
        })
        .catch(function (error) {
            console.log(error);
        })
    }
    submit = (e) => {
        confirmAlert({
          title: 'Confirm to Delete',
          message: `Are you sure to Delete.${e}`,
          buttons: [
            {
              label: 'Yes',
              onClick: () =>{
    axios.delete(`http://localhost:9001/delete/${e}`)
            .then(response => {
                this.setState({
                    fileDelete: true
                }, this.getFile())
            })
              } 
            },
            {
              label: 'No',
              onClick: () => {

              }
            }
          ]
        });
      };
    // deleteFile = (e) => {
    //     var value=window.confirm("Are you sure delete this file");
    //     if(value===true){
    //     axios.delete(`http://localhost:9001/delete/${e}`)
    //         .then(response => {
    //             this.setState({
    //                 fileDelete: true
    //             }, this.getFile())
    //         })
    // }}

    render(){
        return(
            <div className="container">
            {/* <h3>Files</h3> */}
              <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>File Name</th>
                            <th>Size</th>
                            <th>Date</th>
                            <th>View/Download</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.items.map((filelist,i) => 
                            <tr key={i}>
                                <td>{filelist.name}</td>
                                <td>{filesize(filelist.size,{round: 0})}</td>
                                 <td>{filelist.date}</td> 
                                 <td><Link to={{ pathname:`https://storage.googleapis.com/${this.state.bucket}/${filelist.name}`}} target="_blank" download><button type="button" className="btn btn-success">View/Download</button></Link></td>
                                  {/* <td><button type="button" className="btn btn-danger" size="sm" onClick={e => this.deleteFile(filelist.name)}>Delete</button></td> */}
                                  <td><button className="btn btn-danger" size="sm" onClick={e=>this.submit(filelist.name)}>Delete</button></td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ListFiles;